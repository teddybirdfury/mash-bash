﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackTypes {

    //Attack types are based on button values
    public enum ATTACK_TYPES
    {
        PUNCH,
        KICK,
        SPECIAL,
    }
}
