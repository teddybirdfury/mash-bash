﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    public static UIManager Instance;

    public Transform[] UiViews;
    public enum UI_VIEW
    {
        TITLE_SCREEN,
        GAME_VIEW
    }

    public Transform[] m_gameModeViews;
    public enum GAME_MODE_VIEW
    {
        MODE_A,
        MODE_B,
        MODE_C,
    }

    public enum SIMON_BUTTON_TYPES
    {
        PUNCH, //Purple
        KICK,   //Blue
        SPECIAL,    //Green
        INCORRECT   //Black
    }

    public TimerBar timerBar;
    public HealthBar playerHealth;
    public HealthBar enemyHealth;
    public HitComboUI hitCombo;
    public UltraBar ultraBar;
    public UltraButton ultraButton;
    public WatchPrompt promptText;
    public DamageText damageText;
    public GameObject gameOver;
    public GameObject m_youWin;
    public GameObject m_youLose;
    public FadeUI m_missText;
    public GameObject restartButton;
    public DamageModifierText m_damageModifier;
    public Transform m_ultraHitsText;
    public GameObject m_ultraDamageModal;
    public Sprite m_ultraIcon;

    public PhasesUI m_phaseUI;
    public GameObject m_nextRoundUI;
    public TextMeshProUGUI m_nextRoundText;

    #region Button Animations for mode B
    // public bool m_startButtonAnimationModeB = false;
    public SimonButton[] m_buttonInteractionsModeB;
    public Transform[] m_buttonRowsModeB;
    public SimonButton[] m_modeBSimonButtons;
    public List<SimonButton> m_simonButtonReference;
    private int m_buttonRowIndex = 0;
    private List<Vector3> m_defaultButtonPositionModeB;
    private List<Vector3> m_defaultButtonScaleModeB;
    private List<Vector3> m_targetButtonPositionsModeB;
    private List<Vector3> m_targetButtonScaleModeB;
    private List<Vector3> m_buttonRowsModeBCurrentPosition;
    private List<Vector3> m_buttonRowsModeBCurrentScale;
    #endregion

    private void Awake()
    {
        Instance = this;
    }

    // Use this for initialization
    void Start () {
        ActivateTimerBar(false);
        InitDefaultPositions();
    }

    public void TitleView()
    {
        UiViews[(int)UI_VIEW.GAME_VIEW].gameObject.SetActive(false);
        UiViews[(int)UI_VIEW.TITLE_SCREEN].gameObject.SetActive(true);
    }

    public void GameView()
    {
        UiViews[(int)UI_VIEW.GAME_VIEW].gameObject.SetActive(true);
        UiViews[(int)UI_VIEW.TITLE_SCREEN].gameObject.SetActive(false);
    }

    public void HitEnemy()
    {
        //enemyHealth.TakeHit();
    }

    public void UpdatePlayerHealthText(float _value)
    {
        playerHealth.UpdateHealthText(_value);
    }

    public void UpdateEnemyHealthText(float _value)
    {
        enemyHealth.UpdateHealthText(_value);
    }

    public void SetGameOver(bool _winner)
    {
        gameOver.SetActive(true);
        if (_winner)
        {
            m_youWin.SetActive(true);
        }
        else
        {
            m_youLose.SetActive(true);
            playerHealth.UpdateHealthBar(0, 100);
        }
       
    }

    public void ResetUI(int _mode)
    {
        ResetDefaultPositions();

        if (_mode == 1)
        {
            for (int i = 0; i < m_buttonRowsModeB.Length; i++)
            {
                m_buttonRowsModeB[i].localPosition = m_defaultButtonPositionModeB[i];
                m_buttonRowsModeB[i].localScale = m_defaultButtonScaleModeB[i];
            }
        }
        gameOver.SetActive(false);
        m_youWin.SetActive(false);
        m_youLose.SetActive(false);
        m_buttonRowIndex = 0;
    }

    public void ShowMiss()
    {
        m_missText.ShowText();
        m_missText.StartFade();
    }

    public void UltraMode(bool modeOn)
    {
        UltraTextSetActive(modeOn);
        if (modeOn)
        {
            ultraBar.DrainUltraBar();
        }
        else
        {
            SetUltraLevelText(0);
        }
    }

    #region Mode B UI Code

    public void SetButtonProperty(int _buttonIndex, int _simonButtonRef)
    {
        m_modeBSimonButtons[_buttonIndex].SetProperties(m_simonButtonReference[_simonButtonRef]);
    }

    public void SetButtonInteractableModeB()
    {
        //Disable current button interactable and enable on the next row
        if (m_buttonRowIndex + 1 == m_buttonRowsModeB.Length)
        {
            SetButtonRowInteractive(0);
        }
        else
        {
            SetButtonRowInteractive(m_buttonRowIndex + 1);
        }
    }

    public void ToggleInteractiveButtons(bool _enable)
    {
        for(int i = 0; i < m_buttonInteractionsModeB.Length; i++)
        {
            m_buttonInteractionsModeB[i].SetButtonEnabled(_enable);
        }
    }

    public void ShowNextRoundUI(bool _isShow, int _step, int _roundNumber)
    {
        m_nextRoundUI.SetActive(_isShow);
        switch (_step)
        {
            case 1:
                m_nextRoundText.text = "ROUND " + _roundNumber;
            break;
            case 2:
                m_nextRoundText.text = "FIGHT!";
            break;
        }
    }

    public void ShowNextRoundUI(bool _isShow)
    {
        m_nextRoundUI.SetActive(_isShow);
    }

    public void ToggleUltraModeB(bool _isEnable)
    {
        if (_isEnable)
        {
          
            for (int j = 0; j < m_buttonRowsModeB.Length; j++)
            {
                for (int i = 0; i < 3; i++)
                {
                    // m_ultraModeB[j * 3 + i] = new SimonButton(m_modeBSimonButtons[j * 3 + i]);
                  
                    m_modeBSimonButtons[j * 3 + i].SetProperties(m_simonButtonReference[i], true);
                    m_modeBSimonButtons[j * 3 + i].SetUltraIcon(m_ultraIcon);
                    m_modeBSimonButtons[j * 3 + i].ToggleUltraEffects(true);
                }
            }
        }
        else
        {
            for (int j = 0; j < m_buttonRowsModeB.Length; j++)
            {
                for (int i = 0; i < 3; i++)
                {
                    m_modeBSimonButtons[j * 3 + i].SetUltraIcon(m_ultraIcon);
                    m_modeBSimonButtons[j * 3 + i].SetButtonValue(5);
                    m_modeBSimonButtons[j * 3 + i].ToggleUltraEffects(false);
                }
            }
        }
        SetInteractiveButtonProperties();
    }

    public void IncrementButtonRowIndex()
    {
        m_buttonRowIndex++;
        if (m_buttonRowIndex > 5)
        {
            m_buttonRowIndex = 0;
        }
    }

    public void SetInteractiveButtonProperties()
    {
        int m_nextRowIndex = m_buttonRowIndex + 1;
        if (m_nextRowIndex >= m_buttonRowsModeB.Length)
        {
            m_nextRowIndex -= m_buttonRowsModeB.Length;
        }
        SimonButton[] _buttons = m_buttonRowsModeB[m_nextRowIndex].GetComponentsInChildren<SimonButton>();

        int m_index = m_buttonRowIndex * 3 + 3;

        if (m_index >= m_modeBSimonButtons.Length)
        {
            m_index -= m_modeBSimonButtons.Length;
        }

        for (int i = 0; i < 3; i++)
        {
            m_buttonInteractionsModeB[i].SetProperties(_buttons[i]);
            // m_buttonInteractionsModeB[i].SetProperties(m_modeBSimonButtons[m_index + i]);

        }
    }

    public void SetNextButtonProperties(int _random)
    {
        int m_startIndexOnTopRow = m_buttonRowIndex * 3;
        int m_buttonTarget = (m_startIndexOnTopRow + _random);

        for (int i = m_startIndexOnTopRow; i < m_startIndexOnTopRow + 3; i++)    //Last row of buttons
        {
            if (GameManager.instance.currentState == GameManager.GameState.UltraMode)
            {
                m_modeBSimonButtons[i].SetProperties(m_simonButtonReference[i - m_startIndexOnTopRow], true);
            }
            else
            {
                if (i == (m_buttonTarget))
                {
                m_modeBSimonButtons[i].SetProperties(m_simonButtonReference[_random]);
                }
                else
                {
                    m_modeBSimonButtons[i].SetProperties(m_simonButtonReference[(int)SIMON_BUTTON_TYPES.INCORRECT]);
                }
            }
        }
    }

    private void ResetDefaultPositions()
    {
        m_targetButtonPositionsModeB.Clear();
        m_targetButtonScaleModeB.Clear();
        m_buttonRowsModeBCurrentPosition.Clear();
        m_buttonRowsModeBCurrentScale.Clear();

        for (int i = 0; i < m_buttonRowsModeB.Length; i++)
        {
            Vector2 _defaultPosition = m_defaultButtonPositionModeB[i];
            Vector2 _defaultScale = m_defaultButtonScaleModeB[i];
            m_targetButtonPositionsModeB.Add(_defaultPosition);
            m_targetButtonScaleModeB.Add(_defaultScale);
            m_buttonRowsModeBCurrentPosition.Add(_defaultPosition);
            m_buttonRowsModeBCurrentScale.Add(_defaultScale);
        }
    }

    private void InitDefaultPositions()
    {
        m_defaultButtonPositionModeB = new List<Vector3>();
        m_defaultButtonScaleModeB = new List<Vector3>();
        m_targetButtonPositionsModeB = new List<Vector3>();
        m_targetButtonScaleModeB = new List<Vector3>();
        m_buttonRowsModeBCurrentPosition = new List<Vector3>();
        m_buttonRowsModeBCurrentScale = new List<Vector3>();

        for (int i = 0; i < m_buttonRowsModeB.Length; i++)
        {
            Vector2 _defaultPosition = m_buttonRowsModeB[i].localPosition;
            Vector2 _defaultScale = m_buttonRowsModeB[i].localScale;
            m_defaultButtonPositionModeB.Add(_defaultPosition);
            m_defaultButtonScaleModeB.Add(_defaultScale);
            m_targetButtonPositionsModeB.Add(_defaultPosition);
            m_targetButtonScaleModeB.Add(_defaultScale);
            m_buttonRowsModeBCurrentPosition.Add(_defaultPosition);
            m_buttonRowsModeBCurrentScale.Add(_defaultScale);
        }
    }

    public void MoveAnimationButtons()
    {
        StartCoroutine(IMoveAnimationButtons());
    }

    private IEnumerator IMoveAnimationButtons()
    {
       // m_startButtonAnimationModeB = true;
        float timeToMove = 0.05f;
        var currentPos = transform.position;
        var t = 0f;

        int m_targetIndex = m_buttonRowIndex - 1; 
        if (m_targetIndex == -1)
        {
            m_targetIndex = 5;
        }

        while (t < 1)
        {
            t += Time.deltaTime / timeToMove;
            for (int i = 0; i < m_buttonRowsModeB.Length; i++)
            {
                
                //Make sure the first button row doesn't animate
                if (i == m_targetIndex)
                {
                    m_buttonRowsModeB[m_targetIndex].localPosition = m_targetButtonPositionsModeB[i];
                    m_buttonRowsModeB[m_targetIndex].localScale = m_targetButtonScaleModeB[i];
                } else
                {
                    m_buttonRowsModeB[i].localPosition = Vector3.Lerp(m_buttonRowsModeBCurrentPosition[i], m_targetButtonPositionsModeB[i], t);
                    m_buttonRowsModeB[i].localScale = Vector3.Lerp(m_buttonRowsModeBCurrentScale[i], m_targetButtonScaleModeB[i], t);
                }
            }
            yield return null;
        }
       // m_startButtonAnimationModeB = false;
    }

    public bool SetButtonRowInteractive(int _currentRow)
    {
        Button[] _buttons = m_buttonRowsModeB[_currentRow].GetComponentsInChildren<Button>();
        bool _indexReset = true;

        for (int i = 0; i < _buttons.Length; i++)
        {
            _buttons[i].interactable = false;
        }

        //If current row is max row, reset to 0
        if (_currentRow == m_buttonRowsModeB.Length - 1)
        {
            _buttons = m_buttonRowsModeB[0].GetComponentsInChildren<Button>();
        }
        else
        {
            _buttons = m_buttonRowsModeB[_currentRow + 1].GetComponentsInChildren<Button>();
            _indexReset = false;
        }

        for (int i = 0; i < _buttons.Length; i++)
        {
            _buttons[i].interactable = true;
        }

        return _indexReset;
    }

    public void ModeBMoveButtons()
    {
        Vector3 m_tempPos = m_targetButtonPositionsModeB[m_buttonRowsModeB.Length - 1];
        Vector3 m_tempScale = m_targetButtonScaleModeB[m_buttonRowsModeB.Length - 1];
        for (int i = m_buttonRowsModeB.Length - 1; i >= 0; i--)
        {
            if(i != 0) {
                m_targetButtonPositionsModeB[i] = m_targetButtonPositionsModeB[i - 1];
                m_targetButtonScaleModeB[i] = m_targetButtonScaleModeB[i - 1];
            } else
            {
                m_targetButtonPositionsModeB[i] = m_tempPos;
                m_targetButtonScaleModeB[i] = m_tempScale;
            }
            m_buttonRowsModeBCurrentPosition[i] = m_buttonRowsModeB[i].localPosition;
            m_buttonRowsModeBCurrentScale[i] = m_buttonRowsModeB[i].localScale;
        }
    }

    #endregion

    public void SetDamageModifier(float _modifier)
    {
        m_damageModifier.SetdamageModifierText(_modifier);
    }

    public void HitDamage(int damager)
    {
        damageText.SetDamageText(damager);
    }

    public void SetDamageText(int combo, int ultra, int damage)
    {
        damageText.SetDamageText(combo, ultra, damage);
    }
    public void ActivatePromptText(bool activate)
    {
        promptText.ActivatePromptText(activate);

    }

    public void ShowGameModeView(GAME_MODE_VIEW _gameViewMode)
    {
        for(int i = 0; i < m_gameModeViews.Length; i++)
        {
            m_gameModeViews[i].gameObject.SetActive(false);
        }
        m_gameModeViews[(int)_gameViewMode].gameObject.SetActive(true);
        /*foreach(SimonButton _button in m_modeBSimonButtons)
        {
            _button.Initialize();
        }*/
    }


    public void PromptPrompt(bool prompt)
    {
        if (prompt) promptText.Prompt();
        else promptText.Input();
    }

    public void SetReadyCooldown()
    {
        ultraButton.CoolDown();
    }

    public void SetUltraButtonInteractable(bool interactable)
    {
        ultraButton.SetUltraButtonInteractable(interactable);
    }

    public void UltraTextSetActive(bool ready)
    {
        m_ultraHitsText.gameObject.SetActive(ready);
    }

    public void ShowModeCResult(bool _isSuccess)
    {
        m_phaseUI.ShowModeCResult(_isSuccess);
    }

    public void ShowUltraDamage(bool _isShow)
    {
        m_ultraDamageModal.SetActive(_isShow);
    }


    public void ShowUIPhase(int _phase)
    {
        m_phaseUI.ShowPhase(_phase);
    }

    public void HideUIPhases()
    {
        m_phaseUI.DisableAllObjects();
    }

    public void SetUltraLevelText(int ultraLevel)
    {
        ultraBar.SetUltraLevelText(ultraLevel);
    }

    public void ResetUltraBar()
    {
        ultraBar.ResetUltraBar();
    }

    public void SwitchUltraBar()
    {
        ultraBar.SwitchUltraBar();
    }

    public void AddUltraBar()
    {
        ultraBar.AddUltraBar();
    }

    public void ReduceUltraBar()
    {
        ultraBar.ReduceUltraBar();
    }

    public void UpdateUltraFill(float _fill)
    {
        ultraBar.SetUltraBarFill(_fill);
    }

    public void ActivateDamageModifier(bool activate)
    {
        m_damageModifier.ActivateDamageModifierText(activate);
    }

    public void ActivateHitCombo(bool activate)
    {
        hitCombo.ActivateHitCombo(activate);
    }

    public void UpdateHitCombo(int comboLevel)
    {
        hitCombo.SetHitCombo(comboLevel);
    }

    public void ActivateTimerBar(bool active)
    {
        timerBar.ActivateTimer(active);
    }

    public void UltraEffectsToggle(bool _enable)
    {
        ultraButton.UltraEffectsToggle(_enable);
    }

    public void UltraButtonToggle(bool _enable)
    {
        ultraButton.ToggleUltraFillButton(_enable);
    }
}
